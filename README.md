Na programskom jeziku C# kreirati:
 - Apstraktnu klasu Proizvod za predstavljanje proizvoda u prodavnici čiji su privatni ili zaštićeni
 
 -- atributi:
 
• serijski broj proizvoda,
• masa proizvoda u kilogramima (može da bude realna vrednost).

 U javnom delu klase definisati:
• konstruktor koji inicijalizuje sve privatne atribute,
• apstraktno svojstvo (property) koje vraća naziv proizvoda,
• metodu koja na standardni izlaz prikazuje vrednosti privatnih atributa i naziv proizvoda,
• apstraktnu metodu koja vraća cenu proizvoda,
• metodu koja vraća masu proizvoda.

- Klase Cokolada i KiviUKorpici izvedene iz klase Proizvod.

Javni statički atribut klase Cokolada je:
• cena po komadu,

a javna metoda:
• konstruktor koji postavlja sve atribute osnovne klase Proizvod.

Javni statički atribut klase KiviUKorpici je:
• cena po kilogramu (na osnovu cene po kilogramu i mase računa se ukupna cena
proizvoda),

a javna metoda:
• konstruktor koji postavlja sve atriubute osnovne klase Proizvod.

- Interfejs ZaNaplatu koji sadrži metode za:
• vraćanje iznosa cene,
• vraćanje valute (koristiti enumeraciju za definisanje troslovnog koda valute, npr. RSD, EUR,
USD, itd.).

- Klasu PaketProizvoda koja implementira interfejs ZaNaplatu i služi za grupisanje proizvoda iste
vrste.

 Privatni atributi klase PaketProizvoda su:
• niz proizvoda u paketu,
• maksimalni kapacitet niza,
• trenutni broj elemenata u nizu
• troslovni kod valute.

a javne metode su sledeće:
• konstruktor koji postavlja vrednosti maksimalnog kapaciteta niza i troslovnog koda
valute,
• metoda koja dodaje pojedinačni proizvod u paket proizvoda,
• metoda koja na standardni izlaz prikazuje podatke o svim proizvodima u nizu i
ukupnu cenu paketa i odgovarajuću valutu.

U funkciji main tražiti od korisnika da unese cenu pojedinačne čokolade i cenu kivija po kilogramu.
Kreirati paket od 5 čokolada od po 0.1 kilogram sa valutom RSD i paket od 4 korpice kivija sa valutom
EUR. Masu korpice kivija odrediti kao slučajni broj u opsegu od 0.9 do 1.1 kilograma. Serijske brojeve
zadati po sopstvenom izboru, ali tako da se međusobno razlikuju serijski brojevi različitih proizvoda.
Štampati podatke o proizvodima iz oba paketa. 