﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_proizvod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Unesite cenu jedne cokolade:");
            decimal.TryParse(Console.ReadLine(), out Cokolada.cenaPoKomadu);
             
            Console.WriteLine("Unesite cenu jednog kilograma kivija:");
            decimal.TryParse(Console.ReadLine(), out KiviUKorpici.cenaPoKilogramu);

            PaketProizvoda[] nizPaketaProizvoda = new PaketProizvoda[2];

            nizPaketaProizvoda[0] = new PaketProizvoda(5, Valuta.RSD);
            nizPaketaProizvoda[0].DodavanjeProizvoda();
            


            nizPaketaProizvoda[1] = new PaketProizvoda(4, Valuta.EUR);
            nizPaketaProizvoda[1].DodavanjeProizvoda();


            nizPaketaProizvoda[0].Prikaz();
            Console.WriteLine();

            nizPaketaProizvoda[1].Prikaz();
        }
    }
}
