﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_proizvod
{
    class KiviUKorpici : Proizvod
    {
        public static decimal cenaPoKilogramu;

        public KiviUKorpici(string nazivProizvoda, int serijskiBroj, float masaProizvoda)
            :base(nazivProizvoda,serijskiBroj,masaProizvoda)
        {

        }

        public override string Naziv { get { return base.nazivProizvoda; } }

        public override decimal CenaProzivoda()
        {
            decimal ukupnaCena = cenaPoKilogramu * (decimal)base.masaProizvoda;
            return ukupnaCena;
        }
    }
}
