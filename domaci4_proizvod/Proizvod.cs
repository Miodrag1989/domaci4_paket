﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_proizvod
{
    abstract class Proizvod
    {
        private int serijskiBroj;
        protected float masaProizvoda;
        protected string nazivProizvoda;

        public Proizvod(string nazivProizvoda,int serijskiBroj, float masaProizvoda)
        {
            this.nazivProizvoda = nazivProizvoda;
            this.serijskiBroj = serijskiBroj;
            this.masaProizvoda = masaProizvoda;
        }

        public abstract string Naziv { get; }

        public void Prikazivanje()
        {
            Console.WriteLine($"Proizvod: Naziv {this.Naziv} Serijski broj: {this.serijskiBroj}");
        }

        public abstract decimal CenaProzivoda();

        public float MasaProzivoda()
        {
            return this.masaProizvoda;
        }
    }
}
