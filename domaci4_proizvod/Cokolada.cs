﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_proizvod
{
    class Cokolada : Proizvod
    {

        public static decimal cenaPoKomadu;

        public Cokolada(string nazivProizvoda,int serijskiBroj,float masaProzivoda)
            :base(nazivProizvoda,serijskiBroj,masaProzivoda)
        {

        }


        public override string Naziv { get {return base.nazivProizvoda; } }

        public override decimal CenaProzivoda()
        {
            return cenaPoKomadu;
        }
    }
}
