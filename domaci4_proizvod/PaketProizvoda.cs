﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_proizvod
{
    class PaketProizvoda : IZaNaplatu
    {
        private Proizvod[] nizProizvoda;
        private int maksKapacitetNiza;
        private int trenutniKapacitetNiza;
        private Valuta valuta;

        public PaketProizvoda(int maksKapacitetNiza, Valuta valuta)
        {
            this.maksKapacitetNiza = maksKapacitetNiza;
            this.valuta = valuta;
            this.nizProizvoda = new Proizvod[maksKapacitetNiza];
            this.trenutniKapacitetNiza = 0;
        }

        public void DodavanjeProizvoda()
        {
            Console.WriteLine("Opcije: 1 - Unos cokolade; 2 - Unos Kivija:");
            int.TryParse(Console.ReadLine(), out int opcija);
            switch (opcija)
            {
                case 1:
                    for (int i = 0; i < nizProizvoda.Length; i++)
                    {
                        Console.WriteLine($"Unesite naziv cokolade:");
                        string naziv = Console.ReadLine();
                        Console.WriteLine("Unesite serijski broj cokolade:");
                        int.TryParse(Console.ReadLine(), out int serijskiBroj);
                        nizProizvoda[i] = new Cokolada(naziv, serijskiBroj, 0.1f);
                        
                        this.trenutniKapacitetNiza++;
                    }
                    break;
                case 2:
                    Random m = new Random();
                    float masa = (float)(m.Next(900, 1100) / 1000);
                    for (int i = 0; i < nizProizvoda.Length; i++)
                    {
                        Console.WriteLine($"Unesite naziv kivija:");
                        string naziv = Console.ReadLine();
                        Console.WriteLine("Unesite serijski broj kivija:");
                        int.TryParse(Console.ReadLine(), out int serijskiBroj);
                        nizProizvoda[i] = new KiviUKorpici(naziv, serijskiBroj, masa);
                        
                        this.trenutniKapacitetNiza++;
                    }
                    break;
                default:
                    while(opcija < 1 || opcija > 2)
                    {
                        Console.WriteLine("Opcije: 1 - Unos cokolade; 2 - Unos Kivija:");
                        int.TryParse(Console.ReadLine(), out opcija);
                    }
                    break;
            }
            
        }

        public void Prikaz()
        {
            Console.WriteLine("Proizvodi:");
            for(int i = 0; i < this.trenutniKapacitetNiza; i++)
            {
                nizProizvoda[i].Prikazivanje();
                Console.WriteLine($"Cena: {nizProizvoda[i].CenaProzivoda()} {this.valuta}");
            }
            Console.WriteLine($"Ukupna cena paketa je: {this.IznosCene()} {this.valuta}");
        }
        public decimal IznosCene()
        {
            decimal suma = 0;
            for(int i = 0; i < nizProizvoda.Length; i++)
            {
                suma += nizProizvoda[i].CenaProzivoda();
            }
            return suma;
        }

        public Valuta PovracajValute(Proizvod p)
        {
            if(p is Cokolada)
            {
                this.valuta = Valuta.RSD;
            }
            else if(p is KiviUKorpici)
            {
                this.valuta = Valuta.EUR;
            }
            return this.valuta;
        }
    }
}
