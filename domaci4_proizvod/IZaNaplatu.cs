﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_proizvod
{
    interface IZaNaplatu
    {
        decimal IznosCene();
        Valuta PovracajValute(Proizvod p);
    }
}
